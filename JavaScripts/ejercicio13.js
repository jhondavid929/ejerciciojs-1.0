// Imprimir los números del 2 al 100 pero de 2 en 2 (2,4,6,8 ....100).


let serie2
serie2 = 2

let x
x = 1

while (x <= 50) {
    document.write(" ",serie2)
    serie2 = serie2 + 2
    x = x + 1
}


//Otra manera de hacer el mismo ejercicio//

let serie=2*2

while(serie<=100){
    document.write("<br> <h4>otra manera de hacer el mismo ejercicio ",serie)
    serie=serie+2
}

