// Mostrar los múltiplos de 8 hasta el valor 500. Debe aparecer en pantalla 8 -16 -24, etc.

let y = 8 * 1

while (y <= 500) {
    document.write("<h4>Primer resultado</h4>", y)
    
    y = y + 8
}


//Otra manera de hacer el mismo ejercicio//
var multiplo8;

multiplo8 = 8;

while (multiplo8 <= 500) {
    document.write(" <p> Otra manera de hacer el mismo ejercicio ", multiplo8);
  
    multiplo8 = multiplo8 + 8;
}