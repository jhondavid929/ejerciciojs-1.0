// Hasta ahora hemos empleado estructuras SECUENCIALES y CONDICIONALES. 
// Existe otro tipo de estructuras tan importantes como las anteriores 
// que son las estructuras REPETITIVAS.

// Una estructura repetitiva permite ejecutar una instrucción o 
// un conjunto de instrucciones varias veces.

// Una ejecución repetitiva de sentencias se caracteriza por:

// - La o las sentencias que se repiten.
// - El test o prueba de condición antes de cada repetición, 
// que motivará que se repitan o no las sentencias.


//En este adgoritmo repetitivo todos los valores saldran en positivo//
var x;
x=1;

while(x<=100){
    document.write(x);
    document.write('<br>');
    //Con esta instrucción (x=x+1) evitamos que el adgoritmo entre en un bucle infinito//
    //ya que el sistema lo reconoceria como  un error y la pagina se detendria//
    x=x+1;
}